package hitbatch

type Hit struct {
	Timestamp   int64	`name:"ts"`
	Cid 		int64	`name:"cid"`
	Url         string	`name:"url"`
	Referer		string	`name:"rf"`
	UserAgent	string  `name:"ua"`
	VisIdGot	int64	`name:"visIdGot"`
	VisIdSet	int64	`name:"visIdSet"`
}

func (hit *Hit) ToMap() map[string]interface{} {
	result := make(map[string]interface{})
	result["ts"] = hit.Timestamp
	result["cid"] = hit.Cid
	result["url"] = hit.Url
	result["rf"] = hit.Referer
	result["ua"] = hit.UserAgent
	result["visIdGot"] = hit.VisIdGot
	result["visIdSet"] = hit.VisIdSet
	return result
}

func (hit *Hit) FromMap(data map[string]interface{}) {
	loadInt64	("ts", &hit.Timestamp, data)
	loadInt64	("cid", &hit.Cid, data)
	loadString	("rf", &hit.Referer, data)
	loadString	("ua", &hit.UserAgent, data)
	loadInt64	("visIdGot", &hit.VisIdGot, data)
	loadInt64	("visIdSet", &hit.VisIdSet, data)
}

func loadString(key string, field *string, data map[string]interface{}) {
	fieldValue, ok := data[key]
	if !ok || fieldValue == nil {
		return
	}
	*field = fieldValue.(string)
}

func loadInt64(key string, field *int64, data map[string]interface{}) {
	fieldValue, ok := data[key]
	if !ok || fieldValue == nil {
		return
	}
	*field = fieldValue.(int64)
}
package hitbatch

import "testing"
import "time"
import "math/rand"
import "fmt"

// import "reflect"

func BenchmarkEncode1(b *testing.B)     { encodeNum(b, 1) }
func BenchmarkEncode10(b *testing.B)    { encodeNum(b, 10) }
func BenchmarkEncode100(b *testing.B)   { encodeNum(b, 100) }
func BenchmarkEncode1000(b *testing.B)  { encodeNum(b, 1000) }
func BenchmarkEncode10000(b *testing.B) { encodeNum(b, 10000) }

func BenchmarkDecode1(b *testing.B)     { decodeNum(b, 1) }
func BenchmarkDecode10(b *testing.B)    { decodeNum(b, 10) }
func BenchmarkDecode100(b *testing.B)   { decodeNum(b, 100) }
func BenchmarkDecode1000(b *testing.B)  { decodeNum(b, 1000) }
func BenchmarkDecode10000(b *testing.B) { decodeNum(b, 10000) }

func TestHitToMap(t *testing.T) {
	var err error
	hit := Hit{}
	hit.Url = "http://google.com/"
	hit.UserAgent = "chrome"
	hit.Timestamp = time.Now().Unix()
	data := hitToMap(hit)
	t.Logf("hitToMap %v", data)
	hit, err = hitFromMap(data)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("hitFormMap %s", hit)
	buf, _ := Encode([]Hit{hit})
	t.Logf("buf len: %s", string(buf))
	hits, _ := Decode(buf)
	t.Logf("Hits %s", hits)
	// field := reflect.TypeOf(hit).Field(0)
	// t.Logf("data: %s", field)
}

func generateHits(num int) (hits []Hit) {
	for i := 0; i < num; i++ {
		hit := Hit{}
		hit.Timestamp = time.Now().Unix()
		hit.Cid = int64(rand.Int())
		hit.Url = fmt.Sprintf("http://%d/%d", rand.Int(), rand.Int())
		hit.Referer = fmt.Sprintf("http://%d/%d", rand.Int(), rand.Int())
		hit.UserAgent = fmt.Sprintf("%d %d %d", rand.Int(), rand.Int(), rand.Int())
		hit.VisIdGot = int64(rand.Int())
		hit.VisIdSet = int64(rand.Int())
		hits = append(hits, hit)
	}
	return
}

func encodeNum(b *testing.B, hitLength int) (data []byte, err error) {
	hits := generateHits(hitLength)
	for i := 0; i < b.N; i++ {
		data, err = Encode(hits)
	}
	if err != nil {
		b.Logf("error %s", err)
	}
	b.Logf("encode x %d test result %d bytes", hitLength, len(data))
	return
}

func decodeNum(b *testing.B, hitLength int) (data []byte, err error) {
	hits := generateHits(hitLength)
	data, _ = Encode(hits)
	for i := 0; i < b.N; i++ {
		hits, err = Decode(data)
	}
	if err != nil {
		b.Logf("error %s", err)
	}
	b.Logf("decode x %d test result %d bytes", hitLength, len(data))
	return
}

package hitbatch

import (
	"github.com/miolini/msgbatch"
	"code.google.com/p/snappy-go/snappy"
	"reflect"
	"strings"
)

func Encode(hits []Hit) (data []byte, err error) {
	var batch msgbatch.Batch
	var hit Hit
	var item map[string]interface{}
	for _, hit = range hits {
		item = hitToMap(hit)		
		batch.Add(item)
	}
	data, err = batch.Encode()
	return
}

func Decode(data []byte) (hits []Hit, err error) {
	var batch msgbatch.Batch
	var hit Hit
	batch, err = msgbatch.Decode(data)
	if err != nil {
		return
	}
	for _, value := range batch.GetValues() {
		hit, err 	= hitFromMap(value)
		if err != nil {
			return
		}
		hits = append(hits, hit)
	}
	return
}

func DecodeRaw(data []byte) (hits []string, err error) {
	data, err = snappy.Decode(nil, data)
	if err != nil {
		return
	}
	hits = strings.Split(string(data), "\n")
	return
}

func hitToMap(hit Hit) (result map[string]interface{}) {
	result = map[string]interface{}{}
	hitType := reflect.TypeOf(hit)
	hitValue := reflect.ValueOf(hit)
	for i := 0; i < hitValue.NumField(); i++ {
		i = i
		fieldType := hitType.Field(i)
		fieldType.Tag = fieldType.Tag
		fieldValue := hitValue.Field(i)
		name := fieldType.Tag.Get("name")
		if name == "" {
			continue
		}
		kind := fieldValue.Kind()
		switch {
		case kind == reflect.String:
			value := fieldValue.String()
			if value != "" {
				result[name] = fieldValue.String()
			}
		case kind == reflect.Int64:
			value := fieldValue.Int()
			if value != 0 {
				result[name] = value
			}
		}
	}
	return
}

func hitFromMap(data map[string]interface{}) (hit Hit, err error) {
	hitType := reflect.TypeOf(&hit).Elem()
	hitValue := reflect.ValueOf(&hit).Elem()
	for i := 0; i < hitValue.NumField(); i++ {
		fieldType := hitType.Field(i)
		fieldValue := hitValue.Field(i)
		name := fieldType.Tag.Get("name")
		if name == "" {
			continue
		}
		value, ok := data[name]
		if !ok || value == "" || value == nil {
			continue
		}
		kind := fieldValue.Kind()
		switch {
		case kind == reflect.String:
			fieldValue.SetString(value.(string))
		case kind == reflect.Int64:
			fieldValue.SetInt(value.(int64))
		}
	}
	return
}
